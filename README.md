Introduction
=============

Pilotage automatique d'un robot:
- 3 capteurs ultrason HC-SR04
- 2 moteurs à cc (chenilles)
- 1 double pont en H de type *L298N*
- 1 carte Arduino Uno

Cablage du robot
=================

> A Faire...		markdown tables generator et gitlab


Architecture du logiciel
========================

Diagramme de flux
------------------

```mermaid
graph TB
  
  subgraph Entrées
	  BP(Bouton poussoir)
	  C1(G)
	  C2(A)
	  C3(D)
  end

  subgraph Sorties
	  L(LED)
	  G(Moteur gauche)
	  D(Moteur droite)
  end

  subgraph Logiciel
	  LCU[Lecteur capteur à ultrason]
	  DBP(Détecteur appuis bouton poussoir)
	  DM[Détermination du mode]
	  AC[Actions en fonction du mode]

	  C1 -->|mesure écho| LCU
	  C2 -->|mesure écho| LCU
	  C3 -->|mesure écho| LCU

	  LCU -->|distance en cm| DM

	  BP -->|état| DBP
	  DBP -->|évènement 'appui'| DM

	  DM -->|mode courant|AC

	  AC -->|vitesse et sens| G
	  AC -->|vitesse et sens| D
	  AC -->|on/off| L
  end	
  
```

Machine d'états (modes)
-----------------------

En fonction des capteurs `AVANT`, `GAUCHE` et `DROITE`, on peut déterminer 
l'état courant à l'aide de la table de vérité suivante :

| AVANT | GAUCHE | DROITE | Capteurs  | Mode     |
|:-----:|:------:|:------:|:---------:|----------|
|   0   |    0   |    0   |     0     | `RUN`    |
|   0   |    0   |    1   |     1     | `GAUCHE` |
|   0   |    1   |    0   |     2     | `DROITE` |
|   0   |    1   |    1   |     3     | `VIT_MOY`|
|   1   |    0   |    0   |     4     | `GAUCHE` |
|   1   |    0   |    1   |     5     | `GAUCHE` |
|   1   |    1   |    0   |     6     | `DROITE` |
|   1   |    1   |    1   |     7     | `GAUCHE` |



| CAPTEURS |  AVANT loin	| GAUCHE l | DROITE l |	AVANT proche |	GAUCHE	p | DROITE p | Mode adapté*     |
|:--------:|:--------------:|:--------:|:--------:|:------------:|:----------:|:--------:|:----------------:|
|  0       |        0	    |     0    |0         |0        	 |0   	      |0	     | RUN              |
|  1       |        0       |     0    |0  	      |0	         | 0	      |0    	 | TSP_G            |
|  2       |        0	    |     0    |0  	      |0             |  0         |X	     | TSP_D            |
|  3       | 	    0       |     0    |0	      |0	         |X	          |X	     | MOY              |
|  4       |        0       |     0	   |0	      |X	         |0  	      |0	     | TSP_G            |
|  5       |        0	    |     0	   |0	      |X	         |0	          |X	     |TSP_G             |
|  6       |        0	    |     0	   |0	      |X	         |X	          |0	     |TSP_D             |
|  7       |        0	    |     0    |0	      |X	         |X	          |X	     |TSP_G             |
|  8       | 	    0       |     0    |X	      |0	         |0           |0	     |TV_G              |
|  9       |        0	    |     0	   |X	      |0	         |0	          |X	     |*imposs D*        |
| 10       |        0	    |     0	   |X	      |0	         |X	          |0	     |TSP_D             |
| 11       |  	    0       |     0	   |X	      |0	         |X	          |X	     |*imposs D*        |
| 12       |  	    0       |     0	   |X	      |X	         |0	          |0	     |TSP_G             |
| 13       |   	    0       |     0    |X	      |X	         |0	          |X	     |*imposs D*        |
| 14       | 	    0       |     0	   |X	      |X	         |X	          |0	     |TSP_D             |
| 15	   |	    0       |     0    |     X    |X	         |X	          |X	     |*imposs D*        |
| 16       |	    0       |     X    |     0    |0	         |0	          |0	     |TV_D              |
| 17       |  	    0       |     X    |     0    |0	         |0	          |X	     |TSP_G             |
| 18	   |	    0       |     X    |     0    |0	         |X	          |0	     |*imposs G*        |
| 19	   |	    0       |     X    |     0    |0	         |X	          |X	     |*imposs G*        |
| 20       |	    0       |     X    |     0    |X	         |0	          |0	     |TSP_D             |
| 21       |	    0       |     X    |     0    |X	         |0	          |X	     |TSP_G             |
| 22	   |  	    0       |     X    |     0    |X	         |X	          |0	     |*imposs G*        |
| 23	   |  	    0       |     X    |     0    |X	         |X	          |X	     |*imposs G*        |
| 24       |        0	  	|     X    |     X    |0	         |0	          |0	     |RUN               |
| 25	   | 	    0	    |     X    |     X    |0	         |0	          |X	     |*imposs D*        |
| 26	   |        0	   	|     X    |     X    |0	         |X	          |0	     |*imposs G*        |
| 27	   | 	    0	    |     X    |     X    |0	         |X	          |X	     |*imposs G, D*     |
| 28       |        0	  	|     X    |     X    |X	         |0	          |0	     |TSP_G             |
| 29	   |        0	   	|     X    |     X    |X	         |0	          |X	     |*imposs D*        |
| 30	   |        0	  	|     X    |     X    |X	         |X	          |0	     |*imposs G*        |
| 31	   |        0	  	|     X    |     X    |X	         |X	          |X	     |*imposs G, D*     |
| 32       |        X	  	|     0    |     0    |       0      |0	          |0	     |TV_G              |
| 33       |        X	  	|     0    |     0    |       0      |0	          |X	     |TSP_G             |
| 34       | 	  	X       |     0    |     0    |       0      |X	          |0	     |TSP_D             |
| 35       |        X	  	|     0    |     0	  |       0      |X	          |X	     |TSP_G             |
| 36	   |        X	   	|     0    |     0    |       X      |0	          |0	     |*imposs A*        |
| 37	   |	    X	    |     0    |     0    |       X      |0	          |X	     |*imposs A*        |
| 38	   |	    X	    |     0    |     0    |       X      |X	          |0	     |*imposs A*        |
| 39	   | 	    X	    |     0    |     0    |       X      |X	          |X	     |*imposs A*        |
| 40       |        X	  	|     0    |     X    |       0      |0	          |0	     |TV_G              |
| 41	   |        X	  	|     0    |     X    |       0      |      0     |X	     |*imposs D*        |
| 42       |        X	   	|     0    |     X    |       0      |      X     |0	     |TSP_D             |
| 43	   |        X	  	|     0    |     X    |       0      |      X     |X	     |*imposs D*        |
| 44	   |	    X	    |     0    |     X    |       X      |      0     |0	     |*imposs A*        |
| 45	   |	    X	    |     0    |     X    |       X      |      0     |X	     |*imposs A, D*     |
| 46	   |        X	  	|     0    |     X    |       X      |      X     |0	     |*imposs A*        |
| 47	   |        X	   	|     0    |     X    |       X      |      X     |     X    |*imposs A, D*     |
| 48       |        X	   	|     X    |     0    |       0      |      0     |     0    |TV_D              |
| 49       |        X	  	|     X    |     0    |       0      |      0     |     X    |TSP_G             |
| 50	   |        X	  	|     X    |     0    |       0      |      X     |     0    |*imposs G*        |
| 51	   |        X	  	|     X    |     0	  |       0      |      X     |     X    |*imposs G*        |
| 52	   |        X	  	|     X    |     0    |       X      |      0     |     0    |*imposs A*        |
| 53	   |        X	  	|     X    |     0    |       X      |      0     |     X    |*imposs A*        |
| 54	   |        X	  	|     X    |     0    |       X      |      X     |     0    |*imposs A, G*     |
| 55	   |        X	   	|     X    |     0    |       X      |      X     |     X    |*imposs A, G*     |
| 56       |        X	   	|     X    |     X    |       0      |      0     |     0    |TSP_G             |
| 57	   |        X	  	|     X    |     X    |       0      |      0     |     X    |*imposs D*        |
| 58	   |        X	  	|     X    |     X    |       0      |      X     |     0    |*imposs G*        |
| 59	   |        X	  	|     X    |     X    |       0      |      X     |     X    |*imposs G, D*     |
| 60	   |	    X	    |     X    |     X    |       X      |      0     |     0    |*imposs A*        |
| 61	   |        X	   	|     X    |     X    |       X      |      0     |     X    |*imposs A, D*     |
| 62	   |        X	   	|     X    |     X    |       X      |      X     |     0    |*imposs A, G*     |
| 63	   |        X       |     X    |     X    |       X      |      X	  |     X    |*imposs A, G, D*  |

* imposs: la combinaison est impossible. la lettre qui suit indique quel capteur(s) ne peut/peuvent donner ce résultat.

Signification des lettres :
- `A` ==> capteur avant
- `G` ==> capteur de gauche
- `D` ==> capteur de droite

Les modes `TSP_` signifient Tourner Sur Place; la lettre qui suit indiquedans quelle direction.  
Les modes `TV_` signifient Tourner Vite. ils font avancer une chenille légèrement plus vite que l'autre.  
Le mode `MOY` fait avancer le robot à vitesse réduite, contrairement au mode `RUN`
	
Les moteurs droite et gauche sont pilotés en fonction du mode courant.

De fait, le tableau est plus grand parce que nous avons 3 colones suplémentaires pour les obstacles loins et proches de chaque capteurs.
Mathématatiquement parlant, il y a 2^6 soit 64 combinaisons mais en réalité beaucoup sont impossibles (un capteur donne une seule valeur,
il ne peut donc pas détecter un obstacle loin et proche en même temps). Si l'on supprime les combinaisons dont c'est le cas, **il n'en reste plus que 27 de valables**.

