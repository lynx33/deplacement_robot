//************************************************
// Exemple d'utilisation module L298
// Commande de 2 moteurs DC
// tiptopboards.com  04 11 2013
//
//**************************************************
#define ENB 10  // Vitesse moteur A (output pwm)
#define IN3  9  // direction AVANT   moteur A
#define IN4  8  // direction ARRIERE moteur A

#define ENA  5  // Vitesse moteur B (output pwm)
#define IN1  7  // direction AVANT   moteur B
#define IN2  6  // direction ARRIERE moteur B

#define BOUTON  2
#define LED    13

//capteur à ultrasons
#define TRIG_D   4
#define ECHO_D   3
#define TRIG_G  11
#define ECHO_G  12
#define TRIG_A  A0
#define ECHO_A  A1
#define TIMEOUT_ULTRASON  25000UL // 25ms = ~8m à 340m/s

#define MODE_STOP     0
#define MODE_MOY      1
#define MODE_TL_D    11
#define MODE_TL_G    21
#define MODE_RUN      2
#define MODE_TV_D    12
#define MODE_TV_G    22
#define MODE_ARRIERE  8
#define MODE_TSP_D   18
#define MODE_TSP_G   28
#define MODE_PILOTAGE_EXTERNE 99


int mode; // Mode courant du robot
#define VITESSE_ARRIERE_D  -255
#define VITESSE_ARRIERE_G  -255

#define VITESSE_ARRET_D       0
#define VITESSE_ARRET_G       0

#define VITESSE_RAPIDE_D    255
#define VITESSE_RAPIDE_G    255

#define VITESSE_MOY_D       100
#define VITESSE_MOY_G       100

#define DISTANCE_SECURITE_CM  20  // distance arret total
#define PETITE_DISTANCE_CM    30 // distance vitesse réduite
#define MOY_DISTANCE_CM       40
#define GRANDE_DISTANCE_CM    50  // distance vitesse moins réduite

typedef enum
{
  CAPTEUR_D_PROCHE = 0x1, // DROIT
  CAPTEUR_G_PROCHE = 0x2, // GAUCHE
  CAPTEUR_A_PROCHE = 0x4, // AVANT
  
  CAPTEUR_D_LOIN = 0x8, // DROIT
  CAPTEUR_G_LOIN = 0x10, // GAUCHE
  CAPTEUR_A_LOIN = 0x20, // AVANT
}
tCapteurProximite;

bool etat_bouton_precedent;

//============================================================
// Initialisation du programme
//============================================================
void setup()
{
  pinMode(ENA,OUTPUT);//Configuré en sorties
  pinMode(ENB,OUTPUT);
  pinMode(IN1,OUTPUT);
  pinMode(IN2,OUTPUT);
  pinMode(IN3,OUTPUT);
  pinMode(IN4,OUTPUT);
  digitalWrite(ENA,LOW);
  digitalWrite(ENB,LOW);//stop driving
  digitalWrite(IN1,LOW);
  digitalWrite(IN2,HIGH);//setting motorA's direction
  digitalWrite(IN3,HIGH);
  digitalWrite(IN4,LOW);//setting motorB's direction
  
  //Configuration Bouton et Led
  
  pinMode(LED,OUTPUT);
  pinMode(BOUTON,INPUT_PULLUP);
  digitalWrite(LED, LOW);
  digitalWrite(BOUTON,HIGH);
  
  
  //Configuration du mode
  mode = MODE_STOP;
  etat_bouton_precedent = digitalRead(BOUTON);
  
  Serial.begin(115200);  //Serial monitor

    //capteur à ultrasons gauche
  pinMode(TRIG_G, OUTPUT);
  digitalWrite(TRIG_G, LOW);
  pinMode(ECHO_G, INPUT);
   //capteur à ultrasons avant
  pinMode(TRIG_A, OUTPUT);
  digitalWrite(TRIG_A, LOW);
  pinMode(ECHO_A, INPUT);
  //capteur à ultrasons droite
  pinMode(TRIG_D, OUTPUT);
  digitalWrite(TRIG_D, LOW);
  pinMode(ECHO_D, INPUT);
}

//============================================================
// Fonction permettant de définir la vitesse des deux moteurs
//============================================================
void set_vitesses(int vitesse_D, int vitesse_G)
{
  //bornage à [-255, 255]
  if(vitesse_D>+255) vitesse_D=+255;
  if(vitesse_D<-255) vitesse_D=-255;
  if(vitesse_G>+255) vitesse_G=+255;
  if(vitesse_G<-255) vitesse_G=-255;
  
  //moteur droit
  if(vitesse_D>0) //marche avant
  {
    digitalWrite(IN1,HIGH);
    digitalWrite(IN2,LOW);    
    analogWrite(ENA,vitesse_D);
  }
  else  //marche arrière
  {
    digitalWrite(IN1,LOW);
    digitalWrite(IN2,HIGH);
    analogWrite(ENA,-vitesse_D);
  }
  
  //moteur gauche
  if(vitesse_G>0) //marche avant
  {
    digitalWrite(IN3,LOW);
    digitalWrite(IN4,HIGH);
    analogWrite(ENB,vitesse_G);
  }
  else //marche arrière
  {
    digitalWrite(IN3,HIGH);
    digitalWrite(IN4,LOW);
    analogWrite(ENB,-vitesse_G);
  }
}

//============================================================
// Fonctions pour lire les 3 capteurs ultrason
//============================================================
long lire_distance_G_cm() //distance gauche
{
  static long ancien_lire_distance_G_cm = 0;
  long distance = lire_distance_cm(TRIG_G, ECHO_G);

  if(distance >= (ancien_lire_distance_G_cm + 3))
  {
    distance = (ancien_lire_distance_G_cm + 3);
  }
  else if(distance < (ancien_lire_distance_G_cm - 3))
  {
    distance = (ancien_lire_distance_G_cm - 3);
  }
  else
  {
    distance = ancien_lire_distance_G_cm;
  }
  
  ancien_lire_distance_G_cm = distance;
  return (distance);
}

long lire_distance_A_cm() //distance avant
{
  static long ancien_lire_distance_A_cm = 0;
  long distance = lire_distance_cm(TRIG_A, ECHO_A);

  if(distance >= (ancien_lire_distance_A_cm + 4))
  {
    distance = (ancien_lire_distance_A_cm + 4);
  }
  else if(distance < (ancien_lire_distance_A_cm - 4))
  {
    distance = (ancien_lire_distance_A_cm - 4);
  }
  else
  {
    distance = ancien_lire_distance_A_cm;
  }

  ancien_lire_distance_A_cm = distance;
  return (distance);
}

long lire_distance_D_cm() //distance droite
{
  static long ancien_lire_distance_D_cm = 0;
  long distance = lire_distance_cm(TRIG_D, ECHO_D);  

  if(distance >= (ancien_lire_distance_D_cm + 3))
  {
    distance = (ancien_lire_distance_D_cm + 3);  
  }
  else if(distance < (ancien_lire_distance_D_cm - 3))
  {
    distance = (ancien_lire_distance_D_cm - 3);
  }
  else
  {
    distance = ancien_lire_distance_D_cm;
  }
  
  ancien_lire_distance_D_cm = distance;
  return (distance);
}

//============================================================
// Fonction interne permettant de lire et corriger un capteur
// ultrason
//============================================================
long lire_distance_cm(int pin_trigger, int pin_echo)
{
  long lecture_echo_us;
  long cm;

  digitalWrite(pin_trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(pin_trigger, LOW);
  lecture_echo_us = pulseIn(pin_echo,HIGH, TIMEOUT_ULTRASON);
  if(lecture_echo_us ==0)
  {
    cm = 200; // pas d'echo = pas d'obstacle
  }
  else
  {
    cm = lecture_echo_us / 58;
  }

  if(cm > 200) cm=200;  // staturation "longue distance"
  
  return cm;

}

//============================================================
// Pilotage des moteurs en fonction du mode
//============================================================
void priseEnCompteDuMode()
{


  switch(mode)
  {
    case MODE_STOP:
      digitalWrite(LED, LOW);
      set_vitesses(VITESSE_ARRET_D, VITESSE_ARRET_G);
      break;

    case MODE_RUN:
      digitalWrite(LED, HIGH);
      set_vitesses(VITESSE_RAPIDE_D, VITESSE_RAPIDE_G);
      break;

    case MODE_MOY:
      digitalWrite(LED, HIGH);
      set_vitesses(VITESSE_MOY_D, VITESSE_MOY_G);
      break;

    case MODE_ARRIERE:
      digitalWrite(LED, HIGH);
      set_vitesses(VITESSE_ARRIERE_G, VITESSE_ARRIERE_D);
      break;

    case MODE_TL_D:
      digitalWrite(LED, HIGH);
      set_vitesses(VITESSE_ARRET_D, VITESSE_MOY_G);
      break;

    case MODE_TL_G:
      digitalWrite(LED, HIGH);
      set_vitesses(VITESSE_MOY_D, VITESSE_ARRET_G);
      break;
      
    case MODE_TV_D:
      digitalWrite(LED, HIGH);
      set_vitesses(VITESSE_MOY_D, VITESSE_RAPIDE_G);
      break;
      
    case MODE_TV_G:
      digitalWrite(LED, HIGH);
      set_vitesses(VITESSE_RAPIDE_D, VITESSE_MOY_G);
      break;

    case MODE_TSP_D:
      digitalWrite(LED, HIGH);
      set_vitesses(VITESSE_ARRIERE_D, VITESSE_RAPIDE_G);
      break;
      
    case MODE_TSP_G:
      digitalWrite(LED, HIGH);
      set_vitesses(VITESSE_RAPIDE_D, VITESSE_ARRIERE_G);
      break;
      
    case MODE_PILOTAGE_EXTERNE:
      digitalWrite(LED, HIGH);
      // vitesse pilotée ailleurs !
      break;
      
   default:
      digitalWrite(LED, HIGH);
      Serial.println("mode inconnu !!!");
  }
}
//============================================================
//dirige le robot "au mieux" en allant où l'obstacle est le plus éloigné
//cette fonction ne sera utilisée que lorsque l'obstacle est plus grand que DISTANCE_SECURITE_CM
//============================================================
void directionAuMieux()
{
  if(true)
  {
    long distance_G_cm = lire_distance_G_cm();
        long distance_A_cm = lire_distance_A_cm();
        long distance_D_cm = lire_distance_D_cm();
  
  if(distance_G_cm < distance_D_cm)
    {
      mode = MODE_TV_D;
    }
    else if(distance_D_cm < distance_G_cm)
    {
      mode = MODE_TV_G;
    }
    else
    {
      mode = MODE_RUN;
    }
  }
}


//============================================================
// Affiche le mode courant
//   - en mode texte (en non pas le numéro)
//   - uniquement si différent du précendent appel
//============================================================
void AfficheMode()
{
  static int old_mode = -1;

  if(mode!=old_mode)
  {
    Serial.print("Mode : ");

    switch(mode)
    {
      case MODE_STOP    :  Serial.println("MODE_STOP");    break;
      case MODE_MOY     :  Serial.println("MODE_MOY");     break;
      case MODE_TL_D    :  Serial.println("MODE_TL_D");    break;
      case MODE_TL_G    :  Serial.println("MODE_TL_G");    break;
      case MODE_RUN     :  Serial.println("MODE_RUN");     break;
      case MODE_TV_D    :  Serial.println("MODE_TV_D");    break;
      case MODE_TV_G    :  Serial.println("MODE_TV_G");    break;
      case MODE_ARRIERE :  Serial.println("MODE_ARRIERE"); break;
      case MODE_TSP_D   :  Serial.println("MODE_TSP_D");   break;
      case MODE_TSP_G   :  Serial.println("MODE_TSP_G");   break;
      case MODE_PILOTAGE_EXTERNE   :  Serial.println("MODE_PILOTAGE_EXTERNE");   break;
      default:
        Serial.println("mode inconnu !!!");
    }

    old_mode = mode;
       //pour vérifier les capteurs
    
  }
  
  if(true)
  {
    long distance_G_cm = lire_distance_G_cm();
    long distance_A_cm = lire_distance_A_cm();
    long distance_D_cm = lire_distance_D_cm();
      //Serial.print("Distance cm - G: ");
    Serial.print(distance_G_cm);
    Serial.print("     ");
      //Serial.print("Distance cm - A: ");
    Serial.print(distance_A_cm);
    Serial.print("     ");
      //Serial.print("D: ");
    Serial.println(distance_D_cm);
  }
}

//============================================================
// Calcul du mode en fonction des 3 distances
// pour suivre bord (gauche)
// - renvoi mode à appliquer
// - prend en paramètres les 3 distances capteurs
//============================================================
int mode_delta_vitesse(long distance_G_cm, long distance_A_cm, long distance_D_cm)
{
  int new_mode = MODE_PILOTAGE_EXTERNE;
    
  static long old_distance_G_cm=100;
  static long old_distance_A_cm=100;
  static long old_distance_D_cm=100;
  
  //on définit les valeurs pour la direction à vitesse de rotation variable
  long delta_vitesse;
  long erreur;
  long vitesse_Variable_G;
  long vitesse_Variable_D;
  long K = 10 ;

  
  if(distance_A_cm < 30)
  {
    vitesse_Variable_G =   255;
    vitesse_Variable_D = - 255;
  }

  else  // asservissement autour de 35cm
  {
    erreur = distance_G_cm - 35;    // positif si trop loin du mur, négatif si trop près du mur
    delta_vitesse = K * erreur;
    
    if(delta_vitesse > 180)
    {
      delta_vitesse = 180;
    }
     
    vitesse_Variable_D = 200 + delta_vitesse;
    vitesse_Variable_G = 200 - delta_vitesse;
    
    if(vitesse_Variable_D > 255)
    {
      vitesse_Variable_D = 255;
    }
        
    if(vitesse_Variable_G > 255)
    {
      vitesse_Variable_G = 255;
    }
  }

    set_vitesses(vitesse_Variable_D, vitesse_Variable_G);
    
    Serial.print(erreur);
    Serial.print(" ");
    Serial.print(vitesse_Variable_G);
    Serial.print(" ");
    Serial.println(vitesse_Variable_D);
    
    return new_mode;

}   


   
//============================================================
// Calcul mode en fonction des 3 distances
// pour rester au centre du couloir
// - renvoi mode à appliquer
// - prend en paramètres les 3 distances capteurs
//============================================================
int mode_centre(long distance_G_cm, long distance_A_cm, long distance_D_cm)
{
  int new_mode = MODE_PILOTAGE_EXTERNE;
    
  static long old_distance_G_cm=100;
  static long old_distance_A_cm=100;
  static long old_distance_D_cm=100;
  
  //on définit les valeurs pour la direction à vitesse de rotation variable
  long delta_vitesse;
  long erreur;
  long vitesse_Variable_G;
  long vitesse_Variable_D;
  long K = 10 ;

  
  if(distance_A_cm < 40)
  {
    new_mode = MODE_STOP;
  }

  else  // asservissement autour du milieu du couloir
  {
    erreur = distance_G_cm - distance_D_cm;    // positif si trop loin du mur gauche, négatif si trop près du mur gauche
    delta_vitesse = K * erreur;
    
    if(delta_vitesse > 180)
    {
      delta_vitesse = 180;
    }
    if(delta_vitesse < -180)
    {
      delta_vitesse = -180;
    }
     
    vitesse_Variable_D = 200 + delta_vitesse;
    vitesse_Variable_G = 200 - delta_vitesse;
    
    if(vitesse_Variable_D > 255)
    {
      vitesse_Variable_D = 255;
    }
        
    if(vitesse_Variable_G > 255)
    {
      vitesse_Variable_G = 255;
    }
  }

    set_vitesses(vitesse_Variable_D, vitesse_Variable_G);
    
  if(false)
  {
    Serial.print(erreur);
    Serial.print(" ");
    Serial.print(vitesse_Variable_G);
    Serial.print(" ");
    Serial.println(vitesse_Variable_D);
  }
    
    return new_mode;

}

//============================================================
// Boucle principale du programme
//============================================================
void loop()
{  
  // Changement du mode par appuie sur le bouton poussoir
  int etat_bouton = digitalRead(BOUTON);
    
  // Detection d'un appuie sur le bouton (précedent=true && courant=false)
  if(etat_bouton_precedent==HIGH && etat_bouton==LOW)
  {
    //changement du mode
    if(mode == MODE_STOP)
    {
      mode = MODE_RUN;
    }
    else
    {
      mode = MODE_STOP;
    }
    //anti-rebond
    delay(200);
  }
  
  //on sauve l'état du bouton pour le coup suivant
  etat_bouton_precedent = etat_bouton;

  //Détermination du mode à utiliser
  if (mode!= MODE_STOP)
  {
    digitalWrite(LED, HIGH);

    //La vitesse à appliquer dépand de la présence d'obstacle devant le robot...
    //on commence donc par lire la distance de l'obstacle avec le capteur à ultrason
    long distance_G_cm = lire_distance_G_cm();
    long distance_A_cm = lire_distance_A_cm();
    long distance_D_cm = lire_distance_D_cm();

    //et on cherche le nouveau mode
    //mode = mode_bord(distance_G_cm, distance_A_cm, distance_D_cm);
    mode = mode_centre(distance_G_cm, distance_A_cm, distance_D_cm);
    //mode = mode_delta_vitesse(distance_G_cm, distance_A_cm, distance_D_cm);
  }

  AfficheMode();
  priseEnCompteDuMode();

  delay(100);

}
