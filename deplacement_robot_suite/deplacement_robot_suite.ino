 //************************************************
// Exemple d'utilisation module L298
// Commande de 2 moteurs DC
// tiptopboards.com  04 11 2013
//
//**************************************************
#define ENB 10  // Vitesse moteur A (output pwm)
#define IN3  9  // direction AVANT   moteur A
#define IN4  8  // direction ARRIERE moteur A

#define ENA  5  // Vitesse moteur B (output pwm)
#define IN1  7  // direction AVANT   moteur B
#define IN2  6  // direction ARRIERE moteur B

#define BOUTON  2
#define LED    13

//capteur à ultrasons
#define TRIG_D   4
#define ECHO_D   3
#define TRIG_G  11
#define ECHO_G  12
#define TRIG_A  A0
#define ECHO_A  A1
#define TIMEOUT_ULTRASON  25000UL // 25ms = ~8m à 340m/s

#define MODE_STOP     0
#define MODE_MOY      1
#define MODE_TL_D    11
#define MODE_TL_G    21
#define MODE_RUN      2
#define MODE_TV_D    12
#define MODE_TV_G    22
#define MODE_ARRIERE  8
#define MODE_TSP_D   18
#define MODE_TSP_G   28


int mode; // Mode courant du robot
#define VITESSE_ARRIERE_D  -255
#define VITESSE_ARRIERE_G  -255

#define VITESSE_ARRET_D       0
#define VITESSE_ARRET_G       0

#define VITESSE_RAPIDE_D    255
#define VITESSE_RAPIDE_G    255

#define VITESSE_MOY_D       155
#define VITESSE_MOY_G       155

#define DISTANCE_SECURITE_CM  20  // distance arret total
#define PETITE_DISTANCE_CM    30  // distance vitesse réduite
#define GRANDE_DISTANCE_CM    50  // distance vitesse moins réduite


bool etat_bouton_precedent;

//============================================================
// Initialisation du programme
//============================================================
void setup()
{
  pinMode(ENA,OUTPUT);//Configuré en sorties
  pinMode(ENB,OUTPUT);
  pinMode(IN1,OUTPUT);
  pinMode(IN2,OUTPUT);
  pinMode(IN3,OUTPUT);
  pinMode(IN4,OUTPUT);
  digitalWrite(ENA,LOW);
  digitalWrite(ENB,LOW);//stop driving
  digitalWrite(IN1,LOW);
  digitalWrite(IN2,HIGH);//setting motorA's direction
  digitalWrite(IN3,HIGH);
  digitalWrite(IN4,LOW);//setting motorB's direction
  
  //Configuration Bouton et Led
  
  pinMode(LED,OUTPUT);
  pinMode(BOUTON,INPUT_PULLUP);
  digitalWrite(LED, LOW);
  digitalWrite(BOUTON,HIGH);
  
  
  //Configuration du mode
  mode = MODE_STOP;
  etat_bouton_precedent = digitalRead(BOUTON);
  
  Serial.begin(115200);  //Serial monitor

    //capteur à ultrasons gauche
  pinMode(TRIG_G, OUTPUT);
  digitalWrite(TRIG_G, LOW);
  pinMode(ECHO_G, INPUT);
   //capteur à ultrasons avant
  pinMode(TRIG_A, OUTPUT);
  digitalWrite(TRIG_A, LOW);
  pinMode(ECHO_A, INPUT);
  //capteur à ultrasons droite
  pinMode(TRIG_D, OUTPUT);
  digitalWrite(TRIG_D, LOW);
  pinMode(ECHO_D, INPUT);
}

//============================================================
// Fonction permettant de définir la vitesse des deux moteurs
//============================================================
void set_vitesses(int vitesse_D, int vitesse_G)
{
  //moteur droit
  if(vitesse_D>0) //marche avant
  {
    digitalWrite(IN1,HIGH);
    digitalWrite(IN2,LOW);    
    analogWrite(ENA,vitesse_D);
  }
  else  //marche arrière
  {
    digitalWrite(IN1,LOW);
    digitalWrite(IN2,HIGH);
    analogWrite(ENA,-vitesse_D);
  }
  
  //moteur gauche
  if(vitesse_G>0) //marche avant
  {
    digitalWrite(IN3,LOW);
    digitalWrite(IN4,HIGH);
    analogWrite(ENB,vitesse_G);
  }
  else //marche arrière
  {
    digitalWrite(IN3,HIGH);
    digitalWrite(IN4,LOW);
    analogWrite(ENB,-vitesse_G);
  }
}

//============================================================
// Fonctions pour lire les 3 capteurs ultrason
//============================================================
long lire_distance_G_cm() //distance gauche
{
  return lire_distance_cm(TRIG_G, ECHO_G);
}
  
long lire_distance_A_cm() //distance avant
{
  return lire_distance_cm(TRIG_A, ECHO_A);
}

long lire_distance_D_cm() //distance droite
{
  return lire_distance_cm(TRIG_D, ECHO_D);
}

//============================================================
// Fonction interne permettant de lire et corriger un capteur
// ultrason
//============================================================
long lire_distance_cm(int pin_trigger, int pin_echo)
{
  long lecture_echo_us;
  long cm;

  digitalWrite(pin_trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(pin_trigger, LOW);
  lecture_echo_us = pulseIn(pin_echo,HIGH, TIMEOUT_ULTRASON);
  if(lecture_echo_us ==0)
  {
    cm = 200; // pas d'echo = pas d'obstacle
  }
  else
  {
    cm = lecture_echo_us / 58;
  }

  if(cm>200) cm=200;  // staturation "longue distance"
  
  return cm;

}

//============================================================
// Pilotage des moteurs en fonction du mode
//============================================================
void priseEnCompteDuMode()
{
  switch(mode)
  {
    case MODE_STOP:
      digitalWrite(LED, LOW);
      set_vitesses(VITESSE_ARRET_D, VITESSE_ARRET_G);
      break;

    case MODE_RUN:
      digitalWrite(LED, HIGH);
      set_vitesses(VITESSE_RAPIDE_D, VITESSE_RAPIDE_G);
      break;

    case MODE_MOY:
      digitalWrite(LED, HIGH);
      set_vitesses(VITESSE_MOY_D, VITESSE_MOY_G);
      break;

    case MODE_ARRIERE:
      digitalWrite(LED, HIGH);
      set_vitesses(VITESSE_ARRIERE_G, VITESSE_ARRIERE_D);
      break;

    case MODE_TL_D:
      digitalWrite(LED, HIGH);
      set_vitesses(VITESSE_ARRET_D, VITESSE_MOY_G);
      break;

    case MODE_TL_G:
      digitalWrite(LED, HIGH);
      set_vitesses(VITESSE_MOY_D, VITESSE_ARRET_G);
      break;
      
    case MODE_TV_D:
      digitalWrite(LED, HIGH);
      set_vitesses(VITESSE_MOY_D, VITESSE_RAPIDE_G);
      break;
      
    case MODE_TV_G:
      digitalWrite(LED, HIGH);
      set_vitesses(VITESSE_RAPIDE_D, VITESSE_MOY_G);
      break;

    case MODE_TSP_D:
      digitalWrite(LED, HIGH);
      set_vitesses(VITESSE_ARRIERE_D, VITESSE_RAPIDE_G);
      break;
      
    case MODE_TSP_G:
      digitalWrite(LED, HIGH);
      set_vitesses(VITESSE_RAPIDE_D, VITESSE_ARRIERE_G);
      break;
      
    default:
      digitalWrite(LED, HIGH);
      Serial.println("mode inconnu !!!");
  }
}

//============================================================
// Affiche le mode courant
//   - en mode texte (en non pas le numéro)
//   - uniquement si différent du précendent appel
//============================================================
void AfficheMode()
{
  static int old_mode = -1;

  if(mode!=old_mode)
  {
    Serial.print("Mode : ");

    switch(mode)
    {
      case MODE_STOP    :  Serial.println("MODE_STOP");    break;
      case MODE_MOY     :  Serial.println("MODE_MOY");     break;
      case MODE_TL_D    :  Serial.println("MODE_TL_D");    break;
      case MODE_TL_G    :  Serial.println("MODE_TL_G");    break;
      case MODE_RUN     :  Serial.println("MODE_RUN");     break;
      case MODE_TV_D    :  Serial.println("MODE_TV_D");    break;
      case MODE_TV_G    :  Serial.println("MODE_TV_G");    break;
      case MODE_ARRIERE :  Serial.println("MODE_ARRIERE"); break;
      case MODE_TSP_D   :  Serial.println("MODE_TSP_D");   break;
      case MODE_TSP_G   :  Serial.println("MODE_TSP_G");   break;
      default:
        Serial.println("mode inconnu !!!");
    }

    old_mode = mode;
  }
      //pour vérifier les capteurs
      if(true)
      {
        long distance_G_cm = lire_distance_G_cm();
        long distance_A_cm = lire_distance_A_cm();
        long distance_D_cm = lire_distance_D_cm();
          //Serial.print("Distance cm - G: ");
        Serial.print(distance_G_cm);
        Serial.print("     ");
          //Serial.print("Distance cm - A: ");
        Serial.print(distance_A_cm);
        Serial.print("     ");
          //Serial.print("D: ");
        Serial.println(distance_D_cm);
      }
}
 

//============================================================
// Boucle principale du programme
//============================================================
void loop()
{  
  // Changement du mode par appuie sur le bouton poussoir
  int etat_bouton = digitalRead(BOUTON);
    
  // Detection d'un appuie sur le bouton (précedent=true && courant=false)
  if(etat_bouton_precedent==HIGH && etat_bouton==LOW)
  {
    //changement du mode
    if(mode == MODE_STOP)
    {
      mode = MODE_RUN;
    }
    else
    {
      mode = MODE_STOP;
    }
    //anti-rebond
    delay(200);
  }
  
  //on sauve l'état du bouton pour le coup suivant
  etat_bouton_precedent = etat_bouton;

  priseEnCompteDuMode();

  //Détermination du mode à utiliser
  if (mode!= MODE_STOP)
  {
    digitalWrite(LED, HIGH);

    //La vitesse à appliquer dépand de la présence d'obstacle devant le robot...
    //on commence donc par lire la distance de l'obstacle avec le capteur à ultrason
    long distance_G_cm = lire_distance_G_cm();
    long distance_A_cm = lire_distance_A_cm();
    long distance_D_cm = lire_distance_D_cm();


    /*
    //affichage des distances des obstacles    
    Serial.print("Distance cm - G: ");
    Serial.print(distance_G_cm);
    Serial.print("     ");
    Serial.print("D: ");
    Serial.println(distance_D_cm);
    */

    //calcule de la variables "capteurs" qui regroupe les infos des 3 capteurs
    int capteurs=0; //indice de ta table de vérité
    if(distance_D_cm < DISTANCE_SECURITE_CM) capteurs += 1 ;
    if(distance_G_cm < DISTANCE_SECURITE_CM) capteurs += 2 ;
    if(distance_A_cm < DISTANCE_SECURITE_CM) capteurs += 4 ;
    if(distance_D_cm >= DISTANCE_SECURITE_CM && distance_D_cm < GRANDE_DISTANCE_CM) capteurs += 8 ;
    if(distance_G_cm >= DISTANCE_SECURITE_CM && distance_G_cm < GRANDE_DISTANCE_CM) capteurs += 16;
    if(distance_A_cm >= DISTANCE_SECURITE_CM && distance_A_cm < GRANDE_DISTANCE_CM) capteurs += 32;
    




    //calcul du mode en fonction de capteurs
    //(table de vérité)
    switch(capteurs)
    {
      case   0      :     mode = MODE_RUN   ;   break;
      case   1      :     mode = MODE_TSP_G ;   break;
      case   2      :     mode = MODE_TSP_D ;   break;
      case   3      :     mode = MODE_MOY   ;   break;
      case   4      :     mode = MODE_TSP_G ;   break;
      case   5      :     mode = MODE_TSP_G ;   break;
      case   6      :     mode = MODE_TSP_D ;   break;
      case   7      :     mode = MODE_TSP_G ;   break;
      case   8      :     mode = MODE_TV_G  ;   break;
      case   10     :     mode = MODE_TSP_D ;   break;
      case   12     :     mode = MODE_TSP_G ;   break;
      case   14     :     mode = MODE_TSP_D ;   break;
      case   16     :     mode = MODE_TV_D  ;   break;
      case   17     :     mode = MODE_TSP_G ;   break;
      case   20     :     mode = MODE_TSP_D ;   break;
      case   21     :     mode = MODE_TSP_G ;   break;
      case   24     :     mode = MODE_RUN   ;   break;
      case   28     :     mode = MODE_TSP_G ;   break;
      case   32     :     mode = MODE_TV_G  ;   break;
      case   33     :     mode = MODE_TSP_G ;   break;
      case   34     :     mode = MODE_TSP_D ;   break;
      case   35     :     mode = MODE_TSP_G ;   break;
      case   40     :     mode = MODE_TV_G  ;   break;
      case   42     :     mode = MODE_TSP_D ;   break;
      case   48     :     mode = MODE_TV_D  ;   break;
      case   49     :     mode = MODE_TSP_G ;   break;
      case   56     :     mode = MODE_TSP_G ;   break;

      
      default:
        Serial.println("valeurs du capteurs inconnu !!!");
    }
  }

  AfficheMode();

  delay(100);

}
  
  
  
  
  
  
 
